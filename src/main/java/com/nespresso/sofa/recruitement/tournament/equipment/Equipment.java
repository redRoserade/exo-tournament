package com.nespresso.sofa.recruitement.tournament.equipment;

/**
 * Created by 0100975 on 14/10/2016.
 */
public abstract class Equipment {
    /**
     * Returns the amount of damage dealt.
     *
     * @return
     */
    public int getDamage() {
        return 0;
    }

    /**
     * Returns the amount of damage blocked.
     *
     * @param hitPointsOfDamage
     * @param weapon
     * @return
     */
    public int defend(int hitPointsOfDamage, Weapon weapon) {
        return 0;
    }
}

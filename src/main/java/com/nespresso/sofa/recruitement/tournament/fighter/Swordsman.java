package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.equipment.*;

import java.util.ArrayList;

/**
 * Created by 0100975 on 14/10/2016.
 */
public class Swordsman extends Fighter implements Equipable<Swordsman> {

    private boolean isVicious;
    private int numberOfAttacks;

    public Swordsman() {
        super(100, new Sword());
    }

    @Override
    protected int calculateDamageBeforeDefense() {
        int damage = super.calculateDamageBeforeDefense();

        numberOfAttacks++;

        // A vicious swordsman has 20+ attack on hits 1 and 2
        if (isVicious) {
            if (numberOfAttacks <= 2) {
                damage += 20;
                System.out.println("Poison damage: +20 to " + damage);
            } else {
                System.out.println("No poison left");
            }
        }

        return damage;
    }

    public Swordsman(String type) {
        this();

        if (type.equalsIgnoreCase("vicious")) {
            isVicious = true;
        } else {
            throw new IllegalArgumentException("Unknown type of swordsman");
        }
    }
}

package com.nespresso.sofa.recruitement.tournament.equipment;

/**
 * Created by 0100975 on 14/10/2016.
 */
public class Buckler extends Equipment {
    private boolean canBlock;
    private int hitPoints;

    public Buckler() {
        canBlock = true;
        hitPoints = 3;
    }

    @Override
    public int defend(int hitPointsOfDamage, Weapon weapon) { // Don't like the first arg.
        // TODO: Either the missed block doesn't count towards hit point damage,
        //       or it does...

        // Weapon couldn't attack; nothing to block
        if (hitPointsOfDamage == 0) {
            return 0;
        }

        // Can only defend every other turn, if the shield is not destroyed.
        if (canBlock && hitPoints > 0) {
            canBlock = false;

            // Calculate damage to the buckler
            hitPoints -= Math.min(hitPoints, weapon.damageDealtToBuckler());

            System.out.println("Buckler blocked " + hitPointsOfDamage + " HP. Buckler HP left: " + hitPoints);

            return hitPointsOfDamage;
        }

        System.out.println("Buckler failed to block " + hitPointsOfDamage);

        canBlock = true;

        return 0;
    }
}

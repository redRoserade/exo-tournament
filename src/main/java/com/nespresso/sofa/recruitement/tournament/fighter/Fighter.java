package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.equipment.Equipment;
import com.nespresso.sofa.recruitement.tournament.equipment.Weapon;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by 0100975 on 14/10/2016.
 */
public abstract class Fighter {
    private int hitPoints;
    private ArrayList<Equipment> equipment;
    protected Weapon weapon;

    private Fighter() {
        equipment = new ArrayList<>();
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    /**
     * Creates a Fighter with their weapon of choice.
     * @param weapon
     */
    protected Fighter(int initialHitPoints, Weapon weapon) {
        this();
        this.hitPoints = initialHitPoints;
        this.weapon = weapon;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public int hitPoints() {
        return hitPoints;
    }

    public void engage(Fighter b) {
        // Blow exchange are sequential, A engage B means that A will give the first blow, then B will respond
        // Fight goes on while both fighters' HP > 0

        // Damage dealt to other = base damage + (any additional damage) - damage absorbed

        Objects.requireNonNull(b);

        for (int turn = 1; this.hitPoints() > 0 && b.hitPoints() > 0; turn++) {
            System.out.println("--- TURN " + turn + " START ---");

            System.out.println("A's HP: " + this.hitPoints() + ", B's HP: " + b.hitPoints());

            System.out.println("A, " + this.getClass().getSimpleName() + ", attacks");

            // A deals damage to B
            this.doDamage(b);

            // B responds, deals damage to A (if B is still alive)
            if (b.hitPoints() > 0) {
                System.out.println("B, " + b.getClass().getSimpleName() + ", attacks");
                b.doDamage(this);
            }

            System.out.println("--- TURN " + turn + " END ---");
        }
    }

    protected void doDamage(Fighter to) {
        int damage = this.calculateDamageBeforeDefense();

        System.out.println("Damage before defense: " + damage + " hit points.");

        to.receiveDamage(damage, this.getWeapon());
    }

    protected int calculateDamageBeforeDefense() {
        int weaponDamage = weapon.getDamage();

        System.out.println("Damage from weapon: " + weaponDamage);

        int damageFromEquipment = equipment
                .stream()
                .collect(Collectors.summingInt(Equipment::getDamage));

        System.out.println("Damage from equipment: " + damageFromEquipment);

        return weaponDamage + damageFromEquipment;
    }

    protected void receiveDamage(int damageDealt, Weapon weapon) {
        System.out.println("Receiving " + damageDealt + " HP of damage");

        int actualDamage = damageDealt;

        for (Equipment eq : equipment) {
            actualDamage -= eq.defend(actualDamage, weapon);
        }

        actualDamage = Math.min(actualDamage, this.hitPoints);

        System.out.println("Damage after equipment: " + actualDamage);

        // Even if the damage is greater than the fighter's current HP,
        // the HP cannot fall below zero.

        this.hitPoints -= actualDamage;

        System.out.println("Hit points left: " + this.hitPoints);
    }

    public ArrayList<Equipment> getEquipment() {
        return equipment;
    }
}

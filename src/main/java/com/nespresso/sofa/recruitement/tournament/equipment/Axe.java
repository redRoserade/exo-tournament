package com.nespresso.sofa.recruitement.tournament.equipment;

/**
 * Created by 0100975 on 14/10/2016.
 */
public class Axe extends Weapon {
    @Override
    public int getDamage() {
        return 6;
    }

    @Override
    public int damageDealtToBuckler() {
        return 1;
    }
}

package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.equipment.*;

import java.util.ArrayList;

/**
 * Created by 0100975 on 14/10/2016.
 */
public interface Equipable<T> {

    void setWeapon(Weapon weapon);

    ArrayList<Equipment> getEquipment();

    default T equip(String equipment) {

        switch (equipment) {
            case "buckler":
                getEquipment().add(new Buckler());
                break;
            case "armor":
                getEquipment().add(new Armor());
                break;
            case "axe":
                setWeapon(new Axe());
                break;
            default:
                throw new IllegalArgumentException("Unknown equipment: " + equipment);
        }

        return (T) this;
    }
}

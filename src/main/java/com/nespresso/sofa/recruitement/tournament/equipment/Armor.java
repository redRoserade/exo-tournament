package com.nespresso.sofa.recruitement.tournament.equipment;

/**
 * Armor : reduce all received damages by 3 & reduce delivered damages by one
 * Created by 0100975 on 14/10/2016.
 */
public class Armor extends Equipment {
    @Override
    public int getDamage() {
        return -1;
    }

    @Override
    public int defend(int hitPointsOfDamage, Weapon weapon) {
        System.out.println("Armor reduced damage by 3 to " + Math.max(0, hitPointsOfDamage - 3));
        return Math.min(hitPointsOfDamage, 3);
    }
}

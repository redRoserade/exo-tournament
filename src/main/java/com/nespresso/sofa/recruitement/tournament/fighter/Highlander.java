package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.equipment.Greatsword;

/**
 * Created by 0100975 on 14/10/2016.
 */
public class Highlander extends Fighter {

    private boolean isVeteran;

    public Highlander() {
        super(150, new Greatsword());
    }

    public Highlander(String type) {
        this();

        isVeteran = "veteran".equalsIgnoreCase(type);
    }

    @Override
    protected int calculateDamageBeforeDefense() {
        int damage = super.calculateDamageBeforeDefense();

        if (isVeteran && (hitPoints() <= (150 * 0.3))) {
            damage *= 2;
            System.out.println("Active beserk mode doubles damage to " + damage);
        }

        return damage;
    }
}

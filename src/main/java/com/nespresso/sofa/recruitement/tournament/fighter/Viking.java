package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.equipment.Axe;
import com.nespresso.sofa.recruitement.tournament.equipment.Weapon;

/**
 * Created by 0100975 on 14/10/2016.
 */
public class Viking extends Fighter implements Equipable<Viking> {


    public Viking() {
        super(120, new Axe());
    }
}

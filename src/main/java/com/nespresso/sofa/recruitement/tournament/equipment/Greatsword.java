package com.nespresso.sofa.recruitement.tournament.equipment;

/**
 * Created by 0100975 on 14/10/2016.
 */
public class Greatsword extends Weapon {

    /**
     * A sword can only attack 2 turns out of 3
     */
    private static final int TURNS_TO_TIMEOUT = 3;
    private int numberOfAttacks;

    public Greatsword() {
        numberOfAttacks = 1;
    }

    @Override
    public int getDamage() {

        if (numberOfAttacks++ % TURNS_TO_TIMEOUT == 0) {
            System.out.println("Greatsword cannot deal damage");
            return 0;
        }

        return 12;
    }
}
